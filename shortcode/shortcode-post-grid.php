<?php
use ThePIAddon\ThePIAddonManager;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/**
 * This shortcode for post grid view
 * Default : [tpia-post-grid]
 * Category view: Yes
 * Related post view: Yes       [tpia-post-grid related_post="0"]
 * Post Per Page: Yes           [tpia-post-grid post_per_page="-1"]
 * Order: Yes                   [tpia-post-grid order="ASC"]
 * Date Support: Yes            [tpia-post-grid date=""]
 * Before Date Support: Yes     [tpia-related date_query_before="2021-01-01"]
 * After Date Support: Yes      [tpia-related date_query_after="2020-01-01"]
 * Compaire: Yes                [tpia-related date_query_compare=""]
 * Category View: Yes           [tpia-related category="category"]
 * Taxonomy View: Yes           [tpia-related taxonomy="category"]
 * Terms View: Yes              [tpia-related terms=""]
 * Column Support: Yes          [tpia-related column="3"]
 * Show Post By Post ID: Yes    [tpia-related post__in="(int)"]
 * 
 */



function the_Piaddon_get_categories() {
    $separator = ' ';
    $cat_name_as_class = '';
    $post_type = get_post_type(get_the_ID());   
    $taxonomies = get_object_taxonomies($post_type);   
    $taxonomy_slugs = wp_get_object_terms(get_the_ID(), $taxonomies,  array("fields" => "slugs"));
    
        foreach($taxonomy_slugs as $tax_slug) : 
            $cat_name_as_class .='<span class="'. $tax_slug .'">';
            $cat_name_as_class .= $tax_slug . $separator ; 
            $cat_name_as_class .='</span>';     
        endforeach;
        return trim( $cat_name_as_class, $separator );
     
}



function the_Piaddon_post_grid_shortcode( $atts = array() , $content ) { 

    global $post;
    $defaults = array();
    $conditional_args = array();
    // Shortcode Parameter
    $defaults = array(
        'post_type'                 =>  'post',
        'post__in'                  =>  array(),
        'related_post'              =>  '0',
        'posts_per_page'            =>  -1,
        'category'                  =>  'category',
        'date_query'                =>  array(),
        'date_query_before'         =>  '',
		'date_query_after'          =>  '',
		'date_query_column'         =>  '',
		'date_query_compare'        =>  '',
        'category__in'              =>  '',
        'tax_query'                 =>  array(),
        'taxonomy'                  =>  '',
        'terms'                     =>  '',
        'orderby'                   =>  'date',
        'order'                     =>  'ASC',
        'post_status'               =>  'published',
        'suppress_filters'          =>  true,
        'post__not_in'              =>  '',
        'column'                    =>  3,
    );
   

    $marge_args = wp_parse_args($atts, $defaults );
    
    if ( '1' == $marge_args['related_post'] ) {
        $conditional_args = array(
            'category__in'      => wp_get_post_categories($post->ID),
            'post__not_in'      => array($post->ID),
        );
        
    }
    
    $defaults = wp_parse_args($conditional_args, $defaults );
   
    extract( shortcode_atts( $defaults, $atts ) );

    // Post Args

    $Gridcolumn  = ! empty( $column ) ? $column 	: 3;
    $category  = ! empty( $category ) ? $category 	: '';
    $post__in  = ! empty( $post__in ) ? explode(",",$post__in) 	: array();

    $post_args = array(
        'post_type'                 => $post_type,
        'posts_per_page'            => $posts_per_page,
        'category__in'              => $category__in,
        'orderby'                   => $orderby,
        'order'                     => $order,
        'post_status'               => $post_status,
        'suppress_filters'          => $suppress_filters,
        'post__in'                  => $post__in,
        'post__not_in'              => $post__not_in,
    );

     // display ppost by date.
     if ( ! empty( $date_query_after ) || ! empty( $date_query_before ) ) {
        $post_args['date_query'][] = array(
            array(
                'before'    => $date_query_before,
                'after'     => $date_query_after,
                'compare'   => $date_query_compare,
                'inclusive' => true,
            ),
        );
     }

     if ( ! empty( $taxonomy ) || ! empty( $terms )) {
        $post_args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $terms,
                'operator' => 'IN'
            ),
        );
     }

    ob_start();

    $wp_query = new \WP_Query( $post_args );
    if ( $wp_query->have_posts() ) : ?> 

        <div class="row">
            <?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
                global $post;
                //$post_id = $post->get_id(); ?>
                
                    <div class="pb-5 col-md-<?php echo $Gridcolumn; ?>">
                        <div class="tpia-post-content-box">
                            <?php if( has_post_thumbnail() ) : ?>
                                <div class="tpia-post-thumb">
                                    <a href="<?php echo esc_url( get_permalink() ); ?>">
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                </div>
                            <?php endif ;?>
                            <div class="tpia-content-area">
                                <div class="tpia-post-meta-data">
                                    <a class="tpia-date-meta" href="<?php echo esc_url( get_permalink() ); ?>" ><span><?php echo get_the_date();?></span></a>                       
                                    <a class="tpia-author-meta" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" ><span><?php echo get_the_author(); ?></span></a>                     
                                </div>
                                <div class="tpia-post-title">
                                    <a href="<?php echo esc_url( get_permalink() ); ?>">
                                        <?php echo get_the_title(); ?>
                                    </a>
                                </div>
                                <div class="tpia-post-content">
                                    <?php echo substr( get_the_excerpt(), 0, 100 ) ; ?>
                                </div>
                            </div>
                            <div class="tpia-category">
                                <?php //echo the_Piaddon_get_categories(); ?>
                                <?php if ( $category && is_object_in_taxonomy( get_post_type(), $category ) ) { ?>
                                <?php  $terms = get_the_terms( get_the_ID(), $category );
                                        foreach ( $terms as $term ) {
                                            echo $term_output[] = '<span class="'. $term->slug .'"><a href="' . get_term_link( $term, $category ) . '">' . $term->name . '</a></span>';
                                        }
                                ?>
                                <?php } ;?>
                            </div>
                            <div class="tpia-post-btn">
                                    <a href="<?php echo esc_url( get_permalink() ); ?>" class="tpia-btn"><?php echo esc_html('Read More', 'the-pi-addon') ;?></a>
                            </div>
                        </div>
                    </div>

            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    <?php endif;
    $content .= ob_get_clean();
	return $content;
}

add_shortcode('tpia-post-grid', 'the_Piaddon_post_grid_shortcode');