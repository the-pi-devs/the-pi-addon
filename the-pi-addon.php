<?php
/**
 * Plugin Name:       The PI Addon
 * Plugin URI:        https://thepidevs.com/the-pi-addon
 * Description:       Description of the plugin.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Abdullah
 * Author URI:        https://thepidevs.com
 * Text Domain:       the-pi-addon
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'THEPIADDON__FILE__', __FILE__ );
define( 'THEPIADDON_PLUGIN_BASE', plugin_basename( THEPIADDON__FILE__ ) );
define( 'THEPIADDON_PATH', plugin_dir_path( THEPIADDON__FILE__ ) );
define( 'THEPIADDON_ASSETS_PATH', THEPIADDON_PATH . 'assets/' );
define( 'THEPIADDON_CLASS_PATH', THEPIADDON_PATH . 'class/' );
define( 'THEPIADDON_SHORTCODE_PATH', THEPIADDON_PATH . 'shortcode/' );
define( 'THEPIADDON_FUNCTION_PATH', THEPIADDON_PATH . 'functions/' );
define( 'THEPIADDON_URL', plugins_url( '/', THEPIADDON__FILE__ ) );
define( 'THEPIADDON_ASSETS_URL', THEPIADDON_URL . 'assets/' );


/**
 * Main Post Grid The PI  Addon Class
 *
 * The init class that runs the Post Grid plugin.
 * Intended To make sure that the plugin's minimum requirements are met.
 *
 * You should only modify the constants to match your plugin's needs.
 *
 * Any custom code should go inside Plugin Class in the plugin.php file.
 * @since 1.2.0
 */
final class The_pi_addon {

	/**
	 * Plugin Version
	 *
	 * @since 1.2.0
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '5.6.20';

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {

		// Load translation
		add_action( 'init', array( $this, 'i18n' ) );

		// Init Plugin
		add_action( 'plugins_loaded', array( $this, 'init' ) );
	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 * Fired by `init` action hook.
	 *
	 * @since 1.2.0
	 * @access public
	 */
	public function i18n() {
		load_plugin_textdomain( 'the-pi-addon' );
	}

	/**
	 * Initialize the plugin
	 *
	 * Validates that The PI  is already loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed include the plugin class.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.2.0
	 * @access public
	 */
	public function init() {

		// Once we get here, We have passed all validation checks so we can safely include our plugin
		require_once( 'the-pi-addon-manager.php' );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'post-grid-The PI -addon' ),
			'<strong>' . esc_html__( 'Post Grid The PI  Addon', 'post-grid-The PI -addon' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'post-grid-The PI -addon' ) . '</strong>',
			self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
	}
}

// Instantiate The_pi_addon.
new The_pi_addon();
