<?php
namespace ThePIAddon\ThePIAddonManager;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class category_featured_images
{
	public function __construct()
	{
	// --- Actions ------------------------------------------------------------- //
		add_action( 'admin_print_scripts', array( &$this, 'admin_print_scripts' ) );
		add_action( 'admin_print_styles', array( &$this, 'admin_print_styles' ) );
		add_action( 'category_edit_form', array( &$this, 'category_edit_form' ) );
		add_action( 'edited_category', array( &$this, 'edited_category' ) );
        add_action( 'admin_init', array( &$this, 'taxonomy_image_plugin_add_dynamic_hooks' ) );
	// --- Filters ------------------------------------------------------------- //
		add_filter( 'get_post_metadata', array( &$this, 'get_post_metadata' ), 10, 4 );

	// --- Shortcodes ---------------------------------------------------------- //
		add_shortcode( 'tpai_featured_image', array( &$this, 'show_featured_image' ) );

	// --- Hooks --------------------------------------------------------------- //
		register_uninstall_hook( __FILE__, array( 'category_featured_images', 'uninstall' ) );
	}

	static function get_featured_image_url( $args )
	{
		$size = isset( $args['size'] ) ? $args['size'] : 'full';
		if( is_single() )
		{
			$id = get_post_thumbnail_id();
			if( !empty( $id ) )
			{
				$attachment = wp_get_attachment_image_src( $id, $size );
				if( $attachment !== FALSE ) return $attachment[0];
			}
		}
		else if( is_category() )
		{
			$categories = get_the_category();
			if( $categories )
			{
				foreach( $categories as $category )
				{
					$images = get_option( 'tpai_featured_images' );
					if( isset( $images[$category->term_id] ) )
					{
						$attachment = wp_get_attachment_image_src( $images[$category->term_id], $size );
						if( $attachment !== FALSE ) return $attachment[0];
					}
				}
			}
		}
		return '';
	}

	static function show_featured_image( $args )
	{
		if( isset( $args['size'] ) )
		{
			$size = $args['size'];
			unset( $args['size'] );
		}
		else $size = 'thumbnail';
		if( is_single() )
		{
			$image = get_the_post_thumbnail( null, $size, $args );
			if( !empty( $image ) ) return '<span class="tpai-featured-image">' . $image . '</span>';
		}
		else if( is_category() )
		{
			$categories = get_the_category();
			if( $categories )
			{
				foreach( $categories as $category )
				{
					$images = get_option( 'tpai_featured_images' );
					if( isset( $images[$category->term_id] ) ) return '<span class="tpai-featured-image">' . wp_get_attachment_image( $images[$category->term_id], $size ) . '</span>';
				}
			}
		}
		return '';
	}

	static function uninstall()
	{
		delete_option( 'tpai_featured_images' );
	}

	function admin_print_scripts()
	{
		wp_enqueue_media();
		wp_register_script( 'tpai-scripts', THEPIADDON_URL . 'assets/js/category-featured-images.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'tpai-scripts' );
	}

	function admin_print_styles()
	{
        wp_register_style( 'tpai-styles', THEPIADDON_URL . 'assets/css/tpai-styles.css' );
		wp_enqueue_style( 'tpai-styles' );
	}

	function category_edit_form()
	{
		$tag_ID = $_GET['tag_ID'];
		$images = get_option( 'tpai_featured_images' );
		if( $images === FALSE ) $images = array();
		$image = isset( $images[$tag_ID] ) ? $images[$tag_ID] : '';
	?>
		<table class="form-table">
			<tr class="form-field">
				<th valign="top" scope="row">
					<label>Featured Image</label>
				</th>
				<td>
					<input id="tpai-featured-image" type="hidden" name="tpai_featured_image" readonly="readonly" value="<?php echo $image; ?>" />
					<input id="tpai-remove-image" class="button" type="button" value="Remove image" />
					<input id="tpai-change-image" class="button" type="button" value="Change image" />
					<div id="tpai-thumbnail"><?php if( !empty( $image ) ) echo wp_get_attachment_image( $image ); ?></div>
					<p class="description">Set a featured image for all the post of this category without a featured image.</p>
				</td>
			</tr>
		</table>
	<?php
	}

	function edited_category( $term_id )
	{
		if( isset( $_POST['tpai_featured_image'] ) )
		{
			$images = get_option( 'tpai_featured_images' );
			if( $images === FALSE ) $images = array();
			//$url = trim( $_POST['tpai_featured_image'] );	// URL alternative
			//$images[$term_id] = !empty( $url ) ? esc_url( $url ) : NULL;
			$img_id = trim( $_POST['tpai_featured_image'] );
			$images[$term_id] = !empty( $img_id ) ? $img_id : NULL;
			update_option( 'tpai_featured_images', $images );
		}
	}

	function get_post_metadata( $meta_value, $object_id, $meta_key, $single )
	{
		if( is_admin() || '_thumbnail_id' != $meta_key ) return $meta_value;
	// From: wp-includes/meta.php - get_metadata()
		$meta_type = 'post';
		$meta_cache = wp_cache_get($object_id, $meta_type . '_meta');
		if( !$meta_cache )
		{
			$meta_cache = update_meta_cache( $meta_type, array( $object_id ) );
			$meta_cache = $meta_cache[$object_id];
		}
		if( !$meta_key ) return $meta_cache;
		if( isset($meta_cache[$meta_key]) )
		{
			if( $single ) return maybe_unserialize( $meta_cache[$meta_key][0] );
			else return array_map('maybe_unserialize', $meta_cache[$meta_key]);
		}
		if( $single )
		{
		// Look for a category featured image
			$categories = wp_get_post_categories( $object_id );
			if( isset( $categories[0] ) )
			{
				$images = get_option( 'tpai_featured_images' );
				if( $images !== FALSE && isset( $images[$categories[0]] ) ) return $images[$categories[0]];
			}
			return '';
		}
		else return array();
	}

    /**
     * Edit Term Rows.
     *
     * Create image control for each term row of wp-admin/edit-tags.php.
     *
     * @see taxonomy_image_plugin_add_dynamic_hooks()
     *
     * @param     string    Row.
     * @param     string    Name of the current column.
     * @param     int       Term ID.
     * @return    string    @see taxonomy_image_plugin_control_image()
     *
     * @access    private
     * @since     2010-11-08
     */
    public function taxonomy_image_plugin_taxonomy_rows( $row, $column_name, $term_id ) {
        if ( 'taxonomy_image_plugin' === $column_name ) {
            global $taxonomy;
            return $row . taxonomy_image_plugin_control_image( $term_id, $taxonomy );
        }
        return $row;
    }

    /**
     * Edit Term Columns.
     *
     * Insert a new column on wp-admin/edit-tags.php.
     *
     * @see taxonomy_image_plugin_add_dynamic_hooks()
     *
     * @param     array     A list of columns.
     * @return    array     List of columns with "Images" inserted after the checkbox.
     *
     * @access    private
     * @since     0.4.3
     */
    public function taxonomy_image_plugin_taxonomy_columns( $original_columns ) {
        $new_columns = $original_columns;
        array_splice( $new_columns, 1 );
        $new_columns['taxonomy_image_plugin'] = esc_html__( 'Image', 'the-pi-addon' );
        return array_merge( $new_columns, $original_columns );
    }

    /**
     * Image Control.
     *
     * Creates all image controls on edit-tags.php.
     *
     * @todo      Remove rel tag from link... will need to adjust js to accomodate.
     * @since     0.7
     * @access    private
     */
    public function taxonomy_image_plugin_control_image( $term_id, $taxonomy ) {

        $term = get_term( $term_id, $taxonomy );

        $tt_id = 0;
        if ( isset( $term->term_taxonomy_id ) ) {
            $tt_id = (int) $term->term_taxonomy_id;
        }

        $taxonomy = get_taxonomy( $taxonomy );

        $name = esc_html__( 'term', 'taxonomy-images' );
        if ( isset( $taxonomy->labels->singular_name ) ) {
            $name = strtolower( $taxonomy->labels->singular_name );
        }

        $hide = ' hide';
        $attachment_id = 0;
        $associations = get_featured_image_url();
        if ( isset( $associations[ $tt_id ] ) ) {
            $attachment_id = (int) $associations[ $tt_id ];
            $hide = '';
        }

        $img = taxonomy_image_plugin_get_image_src( $attachment_id );

        $term = get_term( $term_id, $taxonomy->name );

        $o = "\n" . '<div id="' . esc_attr( 'taxonomy-image-control-' . $tt_id ) . '" class="taxonomy-image-control hide-if-no-js">';
        $o.= "\n" . '<a class="thickbox taxonomy-image-thumbnail" href="' . esc_url( admin_url( 'media-upload.php' ) . '?type=image&tab=library&post_id=0&TB_iframe=true' ) . '" title="' . esc_attr( sprintf( __( 'Associate an image with the %1$s named &#8220;%2$s&#8221;.', 'taxonomy-images' ), $name, $term->name ) ) . '"><img id="' . esc_attr( 'taxonomy_image_plugin_' . $tt_id ) . '" src="' . esc_url( $img ) . '" alt="" /></a>';
        $o.= "\n" . '<a class="control upload thickbox" href="' . esc_url( admin_url( 'media-upload.php' ) . '?type=image&tab=type&post_id=0&TB_iframe=true' ) . '" title="' . esc_attr( sprintf( __( 'Upload a new image for this %s.', 'taxonomy-images' ), $name ) ) . '">' . esc_html__( 'Upload.', 'taxonomy-images' ) . '</a>';
        $o.= "\n" . '<a class="control remove' . $hide . '" href="#" id="' . esc_attr( 'remove-' . $tt_id ) . '" rel="' . esc_attr( $tt_id ) . '" title="' . esc_attr( sprintf( __( 'Remove image from this %s.', 'taxonomy-images' ), $name ) ) . '">' . esc_html__( 'Delete', 'taxonomy-images' ) . '</a>';
        $o.= "\n" . '<input type="hidden" class="tt_id" name="' . esc_attr( 'tt_id-' . $tt_id ) . '" value="' . esc_attr( $tt_id ) . '" />';

        $o.= "\n" . '<input type="hidden" class="image_id" name="' . esc_attr( 'image_id-' . $tt_id ) . '" value="' . esc_attr( $attachment_id ) . '" />';

        if ( isset( $term->name ) && isset( $term->slug ) ) {
            $o.= "\n" . '<input type="hidden" class="term_name" name="' . esc_attr( 'term_name-' . $term->slug ) . '" value="' . esc_attr( $term->name ) . '" />';
        }

        $o.= "\n" . '</div>';
        return $o;
    }

    /**
     * Edit Term Control.
     *
     * Create image control for wp-admin/edit-tag-form.php.
     * Hooked into the '{$taxonomy}_edit_form_fields' action.
     *
     * @param     stdClass  Term object.
     * @param     string    Taxonomy slug.
     *
     * @access    private
     * @since     2010-11-08
     */
    public function taxonomy_image_plugin_edit_tag_form( $term, $taxonomy ) {
        $taxonomy = get_taxonomy( $taxonomy );
        $name = __( 'term', 'taxonomy-images' );
        if ( isset( $taxonomy->labels->singular_name ) ) {
            $name = strtolower( $taxonomy->labels->singular_name );
        }
        ?>
        <tr class="form-field hide-if-no-js">
            <th scope="row" valign="top"><label for="description"><?php print esc_html__( 'Image', 'taxonomy-images' ) ?></label></th>
            <td>
                <?php print taxonomy_image_plugin_control_image( $term->term_id, $taxonomy->name ); ?>
                <div class="clear"></div>
                <span class="description"><?php printf( esc_html__( 'Associate an image from your media library to this %1$s.', 'taxonomy-images' ), esc_html( $name ) ); ?></span>
            </td>
        </tr>
        <?php
    }

    
    /**
     * Dynamically create hooks for each taxonomy.
     *
     * Adds hooks for each taxonomy that the user has given
     * an image interface to via settings page. These hooks
     * enable the image interface on wp-admin/edit-tags.php.
     *
     * @access    private
     * @since     0.4.3
     * @alter     0.7
     */
    public function taxonomy_image_plugin_add_dynamic_hooks() {
        $settings = get_option( 'tpai_featured_images' );
        if ( ! isset( $settings['taxonomies'] ) ) {
            return;
        }
        foreach ( $settings['taxonomies'] as $taxonomy ) {
            add_filter( 'manage_' . $taxonomy . '_custom_column', 'taxonomy_image_plugin_taxonomy_rows', 15, 3 );
            add_filter( 'manage_edit-' . $taxonomy . '_columns',  'taxonomy_image_plugin_taxonomy_columns' );
            add_action( $taxonomy . '_edit_form_fields',          'taxonomy_image_plugin_edit_tag_form', 10, 2 );
        }
    }
    
}

new category_featured_images();

function tpai_featured_image( $args )
{
	echo category_featured_images::show_featured_image( $args );
}

function tpai_featured_image_url( $args )
{
	return category_featured_images::get_featured_image_url( $args );
}
