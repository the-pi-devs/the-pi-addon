<?php
use ThePIAddon\ThePIAddonManager;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/**
 * This shortcode for post grid view
 * Default : [tpia-post-carousel]
 * Category view: Yes
 * Related post view: Yes       [tpia-post-carousel related_post="0"]
 * Post Per Page: Yes           [tpia-post-carousel post_per_page="-1"]
 * Order: Yes                   [tpia-post-carousel order="ASC"]
 * Date Support: Yes            [tpia-post-carousel date=""]
 * Before Date Support: Yes     [tpia-post-carousel date_query_before="2021-01-01"]
 * After Date Support: Yes      [tpia-post-carousel date_query_after="2020-01-01"]
 * Compaire: Yes                [tpia-post-carousel date_query_compare=""]
 * Category View: Yes           [tpia-post-carousel category="category"]
 * Taxonomy View: Yes           [tpia-post-carousel taxonomy="category"]
 * Terms View: Yes              [tpia-post-carousel terms=""]
 * Column Support: Yes          [tpia-post-carousel column="3"]
 *  Show Post By Post ID: Yes   [tpia-related post__in="(int)"]
 * 
 */



// function the_Piaddon_get_categories() {
//     $separator = ' ';
//     $cat_name_as_class = '';
//     $post_type = get_post_type(get_the_ID());   
//     $taxonomies = get_object_taxonomies($post_type);   
//     $taxonomy_slugs = wp_get_object_terms(get_the_ID(), $taxonomies,  array("fields" => "slugs"));
    
//         foreach($taxonomy_slugs as $tax_slug) : 
//             $cat_name_as_class .='<span class="'. $tax_slug .'">';
//             $cat_name_as_class .= $tax_slug . $separator ; 
//             $cat_name_as_class .='</span>';     
//         endforeach;
//         return trim( $cat_name_as_class, $separator );
     
// }



function the_Piaddon_post_carousel_shortcode( $atts = array() , $content ) { 

    global $post;
    //$defaults = array();
    //$conditional_args = array();

    // Shortcode Parameter
    $atts = extract(shortcode_atts( 
        array(
            'post_type'                 =>  'post',
            'post__in'                  =>  array(),
            'related_post'              =>  '0',
            'posts_per_page'            =>  -1,
            'category'                  =>  'category',
            'date_query'                =>  array(),
            'date_query_before'         =>  '',
            'date_query_after'          =>  '',
            'date_query_column'         =>  '',
            'date_query_compare'        =>  '',
            'category__in'              =>  '',
            'tax_query'                 =>  array(),
            'taxonomy'                  =>  '',
            'terms'                     =>  '',
            'orderby'                   =>  'date',
            'order'                     =>  'ASC',
            'post_status'               =>  'published',
            'suppress_filters'          =>  true,
            'post__not_in'              =>  '',
            //'column'                    =>  3,
            'slidesPerView' 		    =>  3,
            'slidestoscroll' 	        =>  1,
            'loop' 				        =>  true,
            'autoplay'     		        =>  false,
            'autoplay_interval'         =>  3000,
            'delay'                     =>  2500,
            'speed'                     =>  300,
            'pauseOnHover'              =>  false,
            'centeredSlides'            =>  false,
            'spaceBetween'              =>  15,
    
        ) , $atts ));

	// Enqueue required styles & scripts
	//wp_enqueue_style( 'swiper' );
	//wp_enqueue_script( 'swiper' );

    //$marge_args = wp_parse_args($atts, $defaults );
    //$marge_args = array_merge($defaults, $atts );
    
    if ( '1' == $related_post ) {
        
        $atts = extract( shortcode_atts(array(
            'category__in'      => wp_get_post_categories($post->ID),
            'post__not_in'      => array($post->ID),
        ), $atts));
      
    }

    // $defaults = wp_parse_args( $conditional_args, $defaults );
    
    // extract( shortcode_atts( $defaults, $atts ) );

    // Post Args
    //$Gridcolumn  = ! empty( $column ) ? $column 	: 3;
    $category  = ! empty( $category ) ? $category 	: '';
    $post__in  = ! empty( $post__in ) ? explode(",",$post__in) 	: array();

    $post_args = array(
        'post_type'                 => $post_type,
        'posts_per_page'            => $posts_per_page,
        'category__in'              => $category__in,
        'orderby'                   => $orderby,
        'order'                     => $order,
        'post_status'               => $post_status,
        'suppress_filters'          => $suppress_filters,
        'post__in'                  => $post__in,
        'post__not_in'              => $post__not_in,
    );

     // display ppost by date.
     if ( ! empty( $date_query_after ) || ! empty( $date_query_before ) ) {
        $post_args['date_query'][] = array(
            array(
                'before'    => $date_query_before,
                'after'     => $date_query_after,
                'compare'   => $date_query_compare,
                'inclusive' => true,
            ),
        );
     }

     if ( ! empty( $taxonomy ) || ! empty( $terms )) {
        $post_args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $terms,
                'operator' => 'IN'
            ),
        );
     }

    //  $atts = shortcode_atts(
    //    array(

    //   ), $atts);
    //$atts = array_merge($atts,$atts);
     $autoplay 			            = ( $autoplay == false ) 		    ? false 				: false;
     $loop 			                = ( $loop == true ) 		        ? true 				    : true;
     $pauseOnHover 			        = ( $pauseOnHover == false ) 		? false 				: true;
     $centeredSlides 			    = ( $centeredSlides == false ) 		? false					: true;
     $speed 			            = ! empty( $speed ) 		        ? $speed 				: 300;
     $delay 			            = ! empty( $delay ) 		        ? $delay			    : 2500;
     $slidesPerView 			    = ! empty( $slidesPerView ) 		? $slidesPerView	    : 3;
     $spaceBetween 			        = ! empty( $spaceBetween ) 		    ? $spaceBetween	        : 15;

     print_r($atts);
     // carousel Parameters 
      $carousel_data_settings = json_encode(
        array_filter([
            "centeredSlides"        =>  $centeredSlides,
            "autoplay"           	=>  $autoplay,
            "delay" 				=>  $delay,
            "loop"           		=>  $loop,
            "speed"       			=>  (int) $speed,
            "pauseOnHover"       	=>  $pauseOnHover,
            "slidesPerView"         =>  (int) $slidesPerView,
            "spaceBetween"   		=>  (int) $spaceBetween,
            "pagination" 			=>  [ 
                "el" 				=> ".swiper-pagination",
                "clickable"  		=> true,
            ],
            "navigation" => [
                "nextEl" => ".swiper-button-next",
                "prevEl" => ".swiper-button-prev",
            ],

        ])
    );

    //var_dump($carousel_data_settings);
//carousel Parameters End
//$slider_conf = compact('slidesPerView', 'centeredSlides', 'loop', 'autoplay', 'delay', 'speed', 'spaceBetween');
    ob_start();

    $wp_query = new \WP_Query( $post_args );
    if ( $wp_query->have_posts() ) : ?> 

        <div class="row2">
            <div class="post-grid-carousel-wrapper ">
                <div class="swiper-container post-grid-carousel swiper-init" data-swiper="<?php echo esc_attr($carousel_data_settings) ;?>">
                    <div class="swiper-wrapper">
                        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post();
                            global $post;
                            //$post_id = $post->get_id(); ?>
                                <div class="swiper-slide">
                                    <div class="pb-5">
                                        <div class="tpia-post-content-box">
                                            <?php if( has_post_thumbnail() ) : ?>
                                                <div class="tpia-post-thumb">
                                                    <a href="<?php echo esc_url( get_permalink() ); ?>">
                                                        <?php the_post_thumbnail(); ?>
                                                    </a>
                                                </div>
                                            <?php endif ;?>
                                            <div class="tpia-content-area">
                                                <div class="tpia-post-meta-data">
                                                    <a class="tpia-date-meta" href="<?php echo esc_url( get_permalink() ); ?>" ><span><?php echo get_the_date();?></span></a>                       
                                                    <a class="tpia-author-meta" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" ><span><?php echo get_the_author(); ?></span></a>                     
                                                </div>
                                                <div class="tpia-post-title">
                                                    <a href="<?php echo esc_url( get_permalink() ); ?>">
                                                        <?php echo get_the_title(); ?>
                                                    </a>
                                                </div>
                                                <div class="tpia-post-content">
                                                    <?php echo substr( get_the_excerpt(), 0, 100 ) ; ?>
                                                </div>
                                            </div>
                                            <div class="tpia-category">
                                                <?php //echo the_Piaddon_get_categories(); ?>
                                                <?php if ( $category && is_object_in_taxonomy( get_post_type(), $category ) ) { ?>
                                                <?php  $terms = get_the_terms( get_the_ID(), $category );
                                                        foreach ( $terms as $term ) {
                                                            echo $term_output[] = '<span class="'. $term->slug .'"><a href="' . get_term_link( $term, $category ) . '">' . $term->name . '</a></span>';
                                                        }
                                                ?>
                                                <?php } ;?>
                                            </div>
                                            <div class="tpia-post-btn">
                                                    <a href="<?php echo esc_url( get_permalink() ); ?>" class="tpia-btn"><?php echo esc_html('Read More', 'the-pi-addon') ;?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>    
                </div>
            </div>
        </div>
    <?php endif;
    $content .= ob_get_clean();
	return $content;
}

add_shortcode('tpia-post-carousel', 'the_Piaddon_post_carousel_shortcode');