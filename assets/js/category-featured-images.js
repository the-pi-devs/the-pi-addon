/**
 * Category Featured Images
 */

jQuery(document).ready( function() {
	var tpai_media_upload;

	jQuery('#tpai-change-image').click(function(e) {
		e.preventDefault();

	// If the uploader object has already been created, reopen the dialog
		if( tpai_media_upload ) {
			tpai_media_upload.open();
			return;
		}

	// Extend the wp.media object
		tpai_media_upload = wp.media.frames.file_frame = wp.media({
			title: 'Choose an image',
			button: { text: 'Choose image' },
			multiple: false
		});
 
	//When a file is selected, grab the URL and set it as the text field's value
		tpai_media_upload.on( 'select', function() {
			attachment = tpai_media_upload.state().get( 'selection' ).first().toJSON();
			jQuery('#tpai-featured-image').val( attachment.id );
			jQuery('#tpai-thumbnail').empty();
			jQuery('#tpai-thumbnail').append( '<img src="' + attachment.url + '" class="attachment-thumbnail tpai-preview" />' );
		});

	//Open the uploader dialog
		tpai_media_upload.open();
	});

	jQuery('#tpai-remove-image').click(function(e) {
		jQuery('#tpai-featured-image').val('');
		jQuery('#tpai-thumbnail').empty();
	});
});
